# Azure Resource Group Name
# Azure Regions reference: https://github.com/claranet/terraform-azurerm-regions/blob/master/regions.tf
variable "resource_group_map" {
  type        = map(any)
  description = "This variable defines the Resource Groups to create with the appropriate location"
  default = {
    "rg-alice-demoapp-dev" : "westus"
    "rg-alice-demoapp-qa" : "centralus"
  }
}

# Add some tags
variable "tags" {
  type = map(string)
  default = {
    env         = "dev"
    TTL         = "48h"
    owner       = "demouser"
    DoNotDelete = "True"
  }
  description = "Tags that should be assigned to the resources in this example"
}
