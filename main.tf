provider "azurerm" {
  features {
  }
}

resource "azurerm_resource_group" "rg" {
  for_each = var.resource_group_map
  name     = each.key
  location = each.value
  tags     = var.tags
}

output "resource_groups" {
  value = {
    for rg in azurerm_resource_group.rg :
    rg.name => rg.location
  }
}
